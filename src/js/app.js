import $ from 'jquery';
import Swiper from 'swiper';
import '../../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min';
import '../../node_modules/slick-carousel/slick/slick.min';
import '../../node_modules/paroller.js/dist/jquery.paroller.min';
import '../../node_modules/jquery-popup-overlay/jquery.popupoverlay';
import '../../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min';
import '../../node_modules/jquery-mask-plugin/dist/jquery.mask.min';
import '../../node_modules/jquery-validation/dist/jquery.validate.min';

$(window).scroll(function() {
  if($(this).scrollTop() > 100) {
    $('body').css('margin-top', 0);
    $('.header__nav_wrapper').addClass('sticky');
  }
  else {
    $('body').css('margin-top', 0);
    $('.header__nav_wrapper').removeClass('sticky');
  }
});
$(window).on('load',function() {
  $('.modal__info').mCustomScrollbar();
});

var swiper = new Swiper('.about-company__slider .swiper-container', {
  pagination: {
    el: '.swiper-pagination',
    type: 'fraction',
    renderFraction: function(currentClass, totalClass) {
      return '<span class="blue-swiper">0</span><span class="' + currentClass + '"></span>' +
        ' / ' +
        '0<span class="' + totalClass + '"></span>';
    }
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});
var swiperTwo = new Swiper('.partners__slider .swiper-container', {
  slidesPerView: 4,
  spaceBetween: 30,
  breakpoints: {
    992: {
      slidesPerView: 3,
      spaceBetween: 40,
      dots: true
    },
    768: {
      slidesPerView: 2,
      spaceBetween: 30,
    },
    500: {
      slidesPerView: 1,
      spaceBetween: 20,
    }
  }
});
var swiperThree = new Swiper('.reviews__block_slider .swiper-container', {
  slidesPerView: 1,
  spaceBetween: 30,
  loop: true,
  autoHeight: true,
  pagination: {
    el: '.swiper-pagination',
    type: 'fraction',
    renderFraction: function(currentClass, totalClass) {
      return '<span class="blue-swiper">0</span><span class="' + currentClass + '"></span>' +
        ' / ' +
        '0<span class="' + totalClass + '"></span>';
    }
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});


function readMore(el) {
  var elem = $(el);
  var fullHeight = elem.innerHeight();
  var maxHeight = 120;
  var moreText = 'Читать весь отзыв';
  var lessText = 'Спрятать';
  var btn = elem.next();

  // $(window).resize(function(event) {
  //   fullHeight = elem.innerHeight();
  //   if (parseInt(elem.css('height'), 10) !== fullHeight && parseInt(elem.css('height'), 10) !== maxHeight) {
  //     elem.css('height', maxHeight).animate({
  //       height: fullHeight,
  //     },
  //     1000, function() {
  //     });
  //   }
  // });

  elem.css({
    height: maxHeight
  });

  btn.click(function(e) {
    e.preventDefault();
    swiperThree.update();

    if (parseInt(elem.css('height'), 10) !== fullHeight) {
      elem.css('height', maxHeight).animate({
        height: fullHeight,
      },
      1000, function() {
        elem.addClass('active');
        btn.html(lessText);
        swiperThree.update();
      });
    }
    else {
      elem.animate({
        height: maxHeight,
      },
      1000, function() {
        elem.css('height', maxHeight);
        elem.removeClass('active');
        btn.html(moreText);
        swiperThree.update();
      });
    }

  });
}
$(window).resize(function() {
  swiperThree.update();
  swiperTwo.update();
  swiper.update();
});

$('.reviews__block_text').each(function(index, elem) {
  swiperThree.update();
  readMore(elem);
});


$(document).ready(function() {

  //--- MODAL ----
  $('.modal').popup({
    transition: 'all 0.3s',
    outline: true, // optional
    focusdelay: 400, // optional
    vertical: 'top', //optional
    // onclose: function() {
    //   $(this).find('label.error').remove();
    // }
  });

  $('.parallax').paroller();

  //--- PRELOADER ---
  $(window).on('load', function() {
    $('.preloader').delay(1000).fadeOut('slow');
  });
  //--- NAV HAMBURGER ---
  $('.nav__btn, .nav a').click(function() {
    $('.nav_top').toggleClass('active'),
    $('.nav__btn').toggleClass('active');
  });
  //---ANCHORN---
  $('nav a').on('click', function(event) {
    var target = $(this.getAttribute('href'));

    if (target.length) {
      event.preventDefault();
      $('html, body').stop().animate({
        scrollTop: target.offset().top
      }, 1000);
    }
  });
  // --- jQuery Mask + jquery VALIDATION ---
  $('input[type="tel"]').mask('+7 (000) 000-00-00');
  jQuery.validator.addMethod('phoneno', function(phone_number, element) {
    return this.optional(element) || phone_number.match(/\+[0-9]{1}\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}/);
  }, 'Введите Ваш телефон');

  $('.form').each(function(index, el) {
    $(el).addClass('form-' + index);

    $('.form-' + index).validate({
      rules: {
        name: 'required',
        agreeRepair: 'required',
        agreeСallback: 'required',
        tel: {
          required: true,
          phoneno: true
        }
      },
      messages: {
        name: 'Введите Ваше имя',
        tel: 'Введите Ваш телефон',
        agreeRepair: 'Нужно соглашение на обработку данных',
        agreeСallback: 'Нужно соглашение на обработку данных'
      },
      submitHandler: function(form) {
        var t = $('.form-' + index).serialize();
        console.log(t);
        ajaxSend('.form-' + index, t);
      }
    });
  });
  function ajaxSend(formName, data) {
    jQuery.ajax({
      type: 'POST',
      url: 'sendmail.php',
      data: data,
      success: function() {
        $('.modal').popup('hide');
        $('#thanks').popup('show');
        setTimeout(function() {
          $(formName).trigger('reset');
        }, 2000);
      }
    });
  };
});
